import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.BasePO;
import pages.LoginPO;
import pages.MainPO;
import pages.WheelPickerPO;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofSeconds;

public class TrainingTest  {

    AppiumDriver driver;
    public int timeoutSec;
    private WebDriverWait wait;
    private TouchAction action;

    private  int count;
    private Dimension screenSize;
    private  int xTop;
    private  int xMiddle;
    private  int xBottom;
    private  int yTop;
    private  int yMiddle;
    private  int yBottom;

    @BeforeMethod
    public void setUp() throws MalformedURLException {

        String apkPath = "src/main/resources/VodQA-Android.apk";
        String avdName = "Pixel_2_API_28";
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.PLATFORM_VERSION,9);
        cap.setCapability(MobileCapabilityType.APP,new File(apkPath).getAbsolutePath());
        cap.setCapability(MobileCapabilityType.DEVICE_NAME,avdName);

        /*
        *   "automationName": "uiautomator2",
          "platformName": "Android",
          "platformVersion": "9",
          "deviceName": "AOSP on IA Emulator",
          "newCommandTimeout": 6000,
          "appWaitDuration": "30000",
          "app": "/Users/mmondragon/Downloads/VodQA-Android.apk",
          "avd": "Pixel_2_API_28"
        *
        * */
        String url = "http://0.0.0.0:4724/wd/hub";

        driver = new AndroidDriver(new URL(url), cap);

        timeoutSec = 30;
        wait = new WebDriverWait(driver, timeoutSec);
        this.action = new TouchAction(this.driver);
        getScreenSize();

        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void loginSuccessfull(){

        MobileElement txtUserName = (MobileElement)driver.findElement(By.xpath("//android.widget.EditText[@content-desc='username']"));
        MobileElement txtPassword = (MobileElement)driver.findElement(By.xpath("//android.widget.EditText[@content-desc='password']"));
        MobileElement btnLogin    = (MobileElement)driver.findElement(By.className("android.widget.Button"));



        txtUserName.sendKeys("admin");
        txtPassword.sendKeys("admin");
        btnLogin.click();

        new WebDriverWait(driver,timeoutSec).until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//android.widget.TextView[@text='Samples List']"))));
        Assert.assertTrue(true);

    }

    @Test
    public void getColorFromSelectWheelPickerDefaultValue(){

        MobileElement txtUserName = (MobileElement)driver.findElement(By.xpath("//android.widget.EditText[@content-desc='username']"));
        MobileElement txtPassword = (MobileElement)driver.findElement(By.xpath("//android.widget.EditText[@content-desc='password']"));
        MobileElement btnLogin    = (MobileElement)driver.findElement(By.className("android.widget.Button"));

        txtUserName.sendKeys("admin");
        txtPassword.sendKeys("admin");
        btnLogin.click();



        scrollDownUntilElementIsVisibleNoParameter();
        ((MobileElement)driver.findElement(By.xpath("//android.widget.TextView[@content-desc='wheelPicker']"))).click();

        new WebDriverWait(driver,timeoutSec).until(ExpectedConditions.visibilityOf((MobileElement)driver.findElement(By.id("android:id/text1"))));
        MobileElement selectColor = (MobileElement)driver.findElement(By.id("android:id/text1"));

        Assert.assertEquals(selectColor.getText(),"red");

    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }


    public void getScreenSize(){
        screenSize = driver.manage().window().getSize();

        //Find swipe x points from screen's with and height.
        //Find xTop point which is at right side of screen.
        xTop = (int) (screenSize.width * 0.10);
        //Find xMiddle point which is at the middle of x axis of screen.
        xMiddle = (int) (screenSize.width * 0.50);
        //Find xBottom point which is at left side of screen.
        xBottom = (int) (screenSize.width * 0.90);

        //Find swipe y points from screen's top and bottom.
        //Find yTop point which is at up side of screen.
        yTop = (int) (screenSize.height * 0.20);
        //Find yMiddle point which is at the middle of y axis of screen.
        yMiddle = (int) (screenSize.height * 0.50);
        //Find yBottom point which is at bottom side of screen.
        yBottom = (int) (screenSize.height * 0.80);

        System.out.println("Screen size: " + screenSize);
        System.out.println("Space available on screen for touch actions: StartX= " + xTop + ", EndX= " + xBottom + ", StartY: " + yTop +", EndY: " + yBottom);

    }

    public <T> void clickElement(T genericElement) {
        MobileElement casted = (MobileElement) genericElement;
        wait.until(ExpectedConditions.elementToBeClickable(casted));
        casted.click();
    }

    public  <T> void sendKeysMobile(T genericElement, String text) {
        MobileElement casted = (MobileElement) genericElement;
        wait.until(ExpectedConditions.elementToBeClickable(casted));
        casted.clear();
        casted.sendKeys(text);

    }

    public void scrollDownUntilElementIsVisibleNoParameter(){
        Function waitUntilElementVisible = new Function<AppiumDriver, Boolean>() {
            @Override
            public Boolean apply(AppiumDriver driver) {

                try {

                    new WebDriverWait(driver, 1).until(ExpectedConditions.visibilityOf((MobileElement)driver.findElement(By.xpath("//android.widget.TextView[@content-desc='wheelPicker']"))));
                    return true;
                }catch(NoSuchElementException | TimeoutException e){
                    swipeUp(20);
                }

                return false;
            };



        };

        wait.until(waitUntilElementVisible);
    }

    public void scrollDownUntilElementIsVisible(MobileElement element){
        Function waitUntilElementVisible = new Function<AppiumDriver, Boolean>() {
            @Override
            public Boolean apply(AppiumDriver driver) {

                try {

                    new WebDriverWait(driver, 1).until(ExpectedConditions.visibilityOf(element));
                    return true;
                }catch(NoSuchElementException | TimeoutException e){
                    swipeUp(20);
                }

                return false;
            };



        };

        wait.until(waitUntilElementVisible);
    }


    public  void swipeUp(double proportion) {

        double swipeUp = yBottom - (yBottom * (proportion/100));

        action.press(point(xBottom /2, yBottom)).waitAction(waitOptions(ofSeconds(1))).moveTo(point(xBottom /2, (int)swipeUp)).release().perform();

    }

}
