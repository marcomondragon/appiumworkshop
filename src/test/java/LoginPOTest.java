import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.LoginPO;
import pages.MainPO;
import pages.WheelPickerPO;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class LoginPOTest {

    AppiumDriver driver;

    @BeforeMethod
    public void setUp() throws MalformedURLException {

        String apkPath = "src/main/resources/VodQA-Android.apk";
        String avdName = "Pixel_2_API_28";
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability(MobileCapabilityType.AUTOMATION_NAME,"uiautomator2");
        cap.setCapability(MobileCapabilityType.PLATFORM_NAME,"Android");
        cap.setCapability(MobileCapabilityType.PLATFORM_VERSION,9);
        cap.setCapability(MobileCapabilityType.APP,new File(apkPath).getAbsolutePath());
        cap.setCapability(MobileCapabilityType.DEVICE_NAME,avdName);

        /*
        *   "automationName": "uiautomator2",
          "platformName": "Android",
          "platformVersion": "9",
          "deviceName": "AOSP on IA Emulator",
          "newCommandTimeout": 6000,
          "appWaitDuration": "3000",
          "app": "/Users/mmondragon/Downloads/VodQA-Android.apk",
          "avd": "Pixel_2_API_28"
        *
        * */
        String url = "http://0.0.0.0:4724/wd/hub";

        driver = new AndroidDriver(new URL(url), cap);
    }

    @Test
    public void loginSuccessfull(){
        LoginPO loginPO = new LoginPO(driver);
        MainPO mainPO   = new MainPO(driver);

        loginPO.enterCredentials();
        mainPO.verifyLoadMainPO();
    }

    @Test
    public void getColorFromSelectWheelPickerDefaultValue(){
        LoginPO loginPO             = new LoginPO(driver);
        MainPO mainPO               = new MainPO(driver);
        WheelPickerPO wheelPickerPO = new WheelPickerPO(driver);

        loginPO.enterCredentials();
        mainPO.selectWheelPicker();
        wheelPickerPO.getOptionTextFromSelectColorPicker();

    }

    @AfterMethod
    public void tearDown(){
        driver.quit();
    }
}
