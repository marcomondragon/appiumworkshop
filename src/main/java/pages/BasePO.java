package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;

import io.appium.java_client.TouchAction;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.time.Duration;
import java.util.function.Function;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static io.appium.java_client.touch.offset.PointOption.point;
import static java.time.Duration.ofSeconds;


public class BasePO {

    AppiumDriver <MobileElement> driver;
    public int timeoutSec;
    public static WebDriverWait wait;
    private TouchAction action;

    private  int count;
    private Dimension screenSize;
    private  int xTop;
    private  int xMiddle;
    private  int xBottom;
    private  int yTop;
    private  int yMiddle;
    private  int yBottom;

    public BasePO(AppiumDriver driver){
        this.driver = driver;
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        timeoutSec = 30;
        wait = new WebDriverWait(driver, timeoutSec);
        this.action = new TouchAction(this.driver);
        getScreenSize();
    }

    public void getScreenSize(){
        screenSize = driver.manage().window().getSize();

        //Find swipe x points from screen's with and height.
        //Find xTop point which is at right side of screen.
        xTop = (int) (screenSize.width * 0.10);
        //Find xMiddle point which is at the middle of x axis of screen.
        xMiddle = (int) (screenSize.width * 0.50);
        //Find xBottom point which is at left side of screen.
        xBottom = (int) (screenSize.width * 0.90);

        //Find swipe y points from screen's top and bottom.
        //Find yTop point which is at up side of screen.
        yTop = (int) (screenSize.height * 0.20);
        //Find yMiddle point which is at the middle of y axis of screen.
        yMiddle = (int) (screenSize.height * 0.50);
        //Find yBottom point which is at bottom side of screen.
        yBottom = (int) (screenSize.height * 0.80);

        System.out.println("Screen size: " + screenSize);
        System.out.println("Space available on screen for touch actions: StartX= " + xTop + ", EndX= " + xBottom + ", StartY: " + yTop +", EndY: " + yBottom);

    }

    public <T> void clickElement(T genericElement) {
        MobileElement casted = (MobileElement) genericElement;
        wait.until(ExpectedConditions.elementToBeClickable(casted));
        casted.click();
    }

    public static <T> void sendKeysMobile(T genericElement, String text) {
        MobileElement casted = (MobileElement) genericElement;
        wait.until(ExpectedConditions.elementToBeClickable(casted));
        casted.clear();
        casted.sendKeys(text);

    }

    public void scrollDownUntilElementIsVisible(MobileElement element){
        Function waitUntilElementVisible = new Function<AppiumDriver, Boolean>() {
            @Override
            public Boolean apply(AppiumDriver driver) {

                try {
                    //element.isDisplayed();
                    new WebDriverWait(driver, 1).until(ExpectedConditions.visibilityOf(element));
                    return true;
                }catch(NoSuchElementException | TimeoutException e){
                    swipeUp(20);
                }

                return false;
            };



        };

        wait.until(waitUntilElementVisible);
    }


    public  void swipeUp(double proportion) {

        double swipeUp = yBottom - (yBottom * (proportion/100));

        action.press(point(xBottom /2, yBottom)).waitAction(waitOptions(ofSeconds(1))).moveTo(point(xBottom /2, (int)swipeUp)).release().perform();

    }




}
