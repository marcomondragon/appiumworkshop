package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class MainPO extends BasePO {

    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Samples List']")
    MobileElement titleHeader;

    @AndroidFindBy(xpath = "//android.widget.TextView[@content-desc='wheelPicker']")
    MobileElement optWheelPicker;

    public MainPO(AppiumDriver driver) {
        super(driver);
    }

    public void verifyLoadMainPO(){
        new WebDriverWait(driver,timeoutSec).until(ExpectedConditions.visibilityOf(titleHeader));
        Assert.assertTrue(true);
        //Assert.assertEquals(titleHeader.getText(),"Samples List");
    }


    public void selectWheelPicker() {
        scrollDownUntilElementIsVisible(optWheelPicker);
        clickElement(optWheelPicker);
    }
}
