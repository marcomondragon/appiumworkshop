package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class WheelPickerPO extends BasePO {

    MobileElement titleColor;

    @AndroidFindBy(id = "android:id/text1")
    MobileElement selectColor;


    public WheelPickerPO(AppiumDriver driver) {
        super(driver);
    }

    public void getOptionTextFromSelectColorPicker() {
        new WebDriverWait(driver, 1).until(ExpectedConditions.visibilityOf(selectColor));
        Assert.assertEquals(selectColor.getText(),"red");

    }
}
